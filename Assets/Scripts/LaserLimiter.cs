﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserLimiter : MonoBehaviour {
    public Transform laser;
    public Transform aimPoint;
    public float maxRange = 400;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        var rayOrigin = transform.position;
        var rayDirection = transform.TransformDirection(Vector3.forward);

        RaycastHit hit;
        float distance = maxRange;
        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, maxRange) ){
            distance = hit.distance;
        }
        laser.localScale = new Vector3(1, 1, distance);
        laser.localPosition = new Vector3(0, 0, distance / 2);
        aimPoint.localPosition = new Vector3(0, 0, distance);
        Debug.DrawRay(rayOrigin, rayDirection * maxRange, Color.white);
    }
}
