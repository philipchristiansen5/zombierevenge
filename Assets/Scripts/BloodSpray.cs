﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class BloodSpray : MonoBehaviour {
    public GameObject[] bloodParticles;
    public int bloodCount = 30;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
    

    private void OnCollisionEnter(Collision collision)
    {
        
        var other = collision.gameObject;
        
        if (other.tag == "Shot")
        {
            var otherRb = other.GetComponent<Rigidbody>();
            var otherDirection = -collision.impulse.normalized;
            for (int i = 0; i < bloodCount; i++)
            {
                var rb = CreateBloodParticle(collision.contacts[0].point + otherDirection * 10 );
                rb.velocity = otherDirection * 30 + Random.insideUnitSphere * 30 + new Vector3(0, 40, 0); ;
            }
            for (int i = 0; i < 3; i++)
            {
                var rb = CreateBloodParticle(collision.contacts[0].point);
                rb.velocity = Vector3.Scale(otherDirection * 20, new Vector3(-1, -1, -1)) + Random.insideUnitSphere * 5 + new Vector3(0, 30, 0);
            }
            transform.parent = null;

            //Remove bullet
            Destroy(other);
            GetComponent<Rigidbody>().useGravity = true;
        }
    }
    

    private Rigidbody CreateBloodParticle(Vector3 collisionPoint)
    {
        int bloodIndex = Random.Range(0, bloodParticles.Length);
        var clone = Instantiate<GameObject>(bloodParticles[bloodIndex], collisionPoint, Quaternion.Euler(0,0,0));
        return clone.GetComponent<Rigidbody>();
        
    }
}
