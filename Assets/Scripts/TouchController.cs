﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TouchController : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler//, IPointerDownHandler, IPointerUpHandler
{
    public float horizontalPos = 0;
    public float verticalPos = 0;

    private Vector2 dragBeginPosition;
    private Vector2 restingAimPosition;
    private float maxDelta;
    public GameObject player;
    public GameObject gun;
    public GameObject gunRotationPoint;
    public Camera cameraView = null;

    private RectTransform rectTransform;
 

    // Use this for initialization
    void Start () {
        rectTransform = GetComponent<RectTransform>();
        restingAimPosition = transform.GetChild(0).position;
        maxDelta = rectTransform.rect.width/2*1.25f;
        horizontalPos = 0;
        verticalPos = 0;
    }
	
	// Update is called once per frame
    
	void Update () {
        AdjustAimMarkerPosition();
        if (cameraView)
        {
            player.transform.Rotate(0, 2*Mathf.Sign(horizontalPos)*Mathf.Pow(horizontalPos,2), 0);
            gunRotationPoint.transform.Rotate(-2*Mathf.Sign(verticalPos) * Mathf.Pow(verticalPos, 2), 0, 0);   
        }
        else
        {
            player.transform.Translate(horizontalPos, 0, verticalPos);
        }
        
	}

    private void AdjustAimMarkerPosition()
    {
        transform.GetChild(0).position = new Vector3(
            restingAimPosition.x + horizontalPos * maxDelta,
            restingAimPosition.y + verticalPos * maxDelta
            );
    }
    

    public void OnDrag(PointerEventData eventData)
    {
        var touchPosition = eventData.position;
        var diffVector = new Vector2(
                Mathf.Clamp(touchPosition.x - dragBeginPosition.x, -maxDelta, maxDelta) / maxDelta,
                Mathf.Clamp(touchPosition.y - dragBeginPosition.y, -maxDelta, maxDelta) / maxDelta
            );

        if(diffVector.magnitude > 1)
        {
            diffVector = (touchPosition - dragBeginPosition).normalized;
        }
        horizontalPos = diffVector.x;
        verticalPos = diffVector.y;
        
        
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        dragBeginPosition = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        horizontalPos = 0;
        verticalPos = 0;
        AdjustAimMarkerPosition();
    }
}
