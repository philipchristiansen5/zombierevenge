﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
    //private Rigidbody rb;
    private int brainCount;
    public float speed;
    public Text brainCountText;

    private void Start()
    {
        //rb = GetComponent<Rigidbody>();
        brainCount = 0;
        UpdateBrainCountText();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //float moveHorizontal = Input.GetAxis("Horizontal");
        //float moveVertical = Input.GetAxis("Vertical");
        //Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        //rb.AddForce(movement);
        //rb.position = rb.position + movement*speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Brain"))
        {
            other.gameObject.SetActive(false);
            brainCount++;
            UpdateBrainCountText();
            
        }
    }

    void UpdateBrainCountText()
    {
        brainCountText.text = "Brain Count: " + this.brainCount;
    }
}
