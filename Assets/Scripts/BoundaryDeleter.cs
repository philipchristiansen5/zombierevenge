﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundaryDeleter : MonoBehaviour {

    public Transform playerTransform;
	
	// Update is called once per frame
	void Update () {
        transform.position = playerTransform.position;
	}

    private void OnTriggerExit(Collider other)
    {
        Destroy(other.gameObject);
    }
}
