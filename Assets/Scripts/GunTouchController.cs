﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GunTouchController : MonoBehaviour, IPointerDownHandler {

    public RectTransform aimRect;
    public RectTransform moveRect;
    public Transform gunSpawn;
    public GameObject shot;
    public int count;

    

    public void OnPointerDown(PointerEventData eventData)
    {
        var clickPos = eventData.position;
        if(!aimRect.rect.Contains(clickPos) && !moveRect.rect.Contains(clickPos))
        {
            Instantiate(shot, gunSpawn.position, gunSpawn.rotation);
        }
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
